from flask import Flask, render_template, request, session
from flask_session import Session

app = Flask(__name__)

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)


@app.route("/", methods=["GET", "POST"])
def index():
    if session.get("notes") is None:
        session["notes"] = []
    if request.method == 'POST':
        note = request.form.get("note")
        session["notes"].append(note)

    headline = "Hello"
    return render_template("index.html", notes=session["notes"])


@app.route("/<string:name>")
def names(name):
    name = name.capitalize()
    return f"Hello {name}!"


@app.route("/test")
def test():
    return "Hello!"


@app.route("/ticker", methods=["GET", "POST"])
def ticker():
    if request.method == 'GET':
        return "Cant do it"
    else:
        name = request.form.get("name")
        return name


if __name__ == "__main__":
    app.run()
